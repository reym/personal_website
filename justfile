default:
    just --list

svelte-dev:
    #!/usr/bin/env bash
    cd svelte-version/
    npm run dev

publish VERSION:
    @echo 'Building {{VERSION}}...'
    @just publish-{{VERSION}}

publish-svelte:
    #!/usr/bin/env nu
    cd svelte-version/
    npm run build
    # if pages exists and we copy build to pages we get ./pages/build
    # instead of a copy of ./build
    if ("pages" | path exists) { rm -rf pages }
    cp -r build/ pages
    cd pages/
    git init --initial-branch main
    git add -A .
    git commit -m "Publish"
    if (git remote -v  | split words | is-empty) { git remote add upstream git@codeberg.org:reym/pages.git }
    # git push -f upstream HEAD:main

    
