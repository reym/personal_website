{
  description = "personal website build with svelte";

  outputs =
    { self, nixpkgs }:
    {

      packages.x86_64-linux.hello = nixpkgs.legacyPackages.x86_64-linux.hello;

      packages.x86_64-linux.default = self.packages.x86_64-linux.hello;

      devShells.x86_64-linux.default = nixpkgs.legacyPackages.x86_64-linux.mkShell {
        buildInputs = with nixpkgs.legacyPackages.x86_64-linux; [
          nodejs
          nodePackages.svelte-language-server
          nodePackages.typescript-language-server
        ];
        # shellHook = "exec nu";
      };

    };
}
