import adapter from '@sveltejs/adapter-static'
import { vitePreprocess } from '@sveltejs/kit/vite'
import { mdsvex } from 'mdsvex'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: [
		vitePreprocess(),
		mdsvex()
	],
	extensions: ['.svelte', '.svx'], // which files are treated as components

	kit: {
		adapter: adapter({
			// fallback: '404.html'
		})
	}
};

export default config;
